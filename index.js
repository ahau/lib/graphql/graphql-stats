const typeDefs = require('./src/typeDefs')
const Resolvers = require('./src/resolvers')

module.exports = ssb => {
  return {
    typeDefs,
    resolvers: Resolvers(ssb)
  }
}
