const tape = require('tape')
const TestBot = require('./test-bot')

tape('CPU load', async t => {
  t.plan(3)
  const { apollo, ssb } = await TestBot()

  const result = await apollo.query({
    query: '{ cpuLoad }'
  })
  t.error(result.errors, 'query should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  t.equals(
    typeof result.data.cpuLoad,
    'number',
    'result.data.cpuLoad is a number'
  )
  ssb.close()
})

tape('RAM Memory load', async t => {
  const { apollo, ssb } = await TestBot()
  t.plan(3)

  const result = await apollo.query({
    query: '{ memoryLoad }'
  })
  t.error(result.errors, 'query should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  t.equals(
    typeof result.data.memoryLoad,
    'number',
    'result.data.memoryLoad is a number'
  )

  ssb.close()
})

tape('Disk usage', async t => {
  const { apollo, ssb } = await TestBot()
  t.plan(6)

  const result = await apollo.query({
    query: '{ diskUsage { use size used fs } }'
  })

  t.error(result.errors, 'query should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  // t.equals(
  //   typeof result.data.diskUsage,
  //   'array',
  //   'result.data.diskUsage is an array'
  // )
  t.equals(
    typeof result.data.diskUsage[0].use,
    'number',
    'result.data.diskUsage[0].use is a number'
  )
  t.equals(
    typeof result.data.diskUsage[0].size,
    'number',
    'result.data.diskUsage[0].size is a number'
  )
  t.equals(
    typeof result.data.diskUsage[0].used,
    'number',
    'result.data.diskUsage[0].used is a number'
  )
  t.equals(
    typeof result.data.diskUsage[0].fs,
    'string',
    'result.data.diskUsage[0].fs is a string'
  )
  ssb.close()
})

tape('Network', async t => {
  // t.plan(5)
  const { apollo, ssb } = await TestBot()

  const result = await apollo.query({
    query: `{
      network {
        ipv4
        ipv6
        publicIpv4
        internetLatency
        portForwarding
      }
    }`
  })
  t.error(result.errors, 'query should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  t.equals(
    typeof result.data.network,
    'object',
    'result.data.network is an object'
  )
  t.equals(
    typeof result.data.network.ipv4,
    'string',
    'result.data.network.ipv4 is a string'
  )
  t.equals(
    typeof result.data.network.internetLatency,
    'number',
    'result.data.network.internetLatency is a number'
  )
  t.equals(
    typeof result.data.network.portForwarding,
    'boolean',
    'result.data.network.portForwarding is a boolean'
  )

  ssb.close()
})
